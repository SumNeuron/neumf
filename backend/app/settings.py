import os, sys

APP_DIR = os.path.dirname(os.path.realpath(__file__))
SECRET_KEY = os.getenv('SECRET_KEY', '!_@M_W3@K_S3cr3T')


LISTEN = ['high', 'default', 'low']

REDIS_URL = os.getenv('REDIS_URL', 'redis://redis:6379')
MAX_TIME_TO_WAIT = 10

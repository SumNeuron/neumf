import redis
from flask_script import Server, Manager
from rq import Worker, Queue, Connection
from uwsgi import app

listen = ['high', 'default', 'low']

manager = Manager(app)
manager.add_command(
    'runserver',
    Server(port=5000, use_debugger=True, use_reloader=True, threaded=False, processes=3)
)


@manager.command
def runworker():
    redis_url = app.config['REDIS_URL']
    redis_connection = redis.from_url(redis_url)
    with Connection(redis_connection):
        worker = Worker(map(Queue, listen))
        worker.work()


if __name__ == '__main__':
    manager.run()

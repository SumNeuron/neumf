import api from '@/plugins/axios.js'


export const checkTaskPromise = async (id) => new Promise(async function(resolve, reject) {
  // do a thing, possibly async, then…
  let response = await api.get(`task/${id}`)
  let taskResult = response.data
  let taskStatus = taskResult.status
  if (taskStatus === 'finished' || taskStatus === 'failed') {
    return resolve(taskResult);
    // return taskResult;
  }
  return setTimeout(()=>{
    return resolve(checkTaskUntilDone(id))
  }, 1000)
});


export const checkTaskUntilDone = async (id) => {
  return await checkTaskPromise(id)
}

# NeuMF

Based on [Neural Collaborative Filtering][NCF] and TensorFlow's implementation
[thereof][TF].


[NCF]: https://arxiv.org/pdf/1708.05031.pdf
[TF]: https://github.com/tensorflow/models/tree/08bb9eb5ad79e6bceffc71aeea6af809cc78694b/official/recommendation
[Metrics]: https://www.comp.nus.edu.sg/~kanmy/papers/cikm15-trirank-cr.pdf

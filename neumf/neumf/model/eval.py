import math
import numpy as np
from neumf.data.transform import scores_to_probabilities

def get_hit_ratio(ranked_items, item_id)->int:
    return 1 if item_id in ranked_items else 0

def get_ndcg(ranked_items, item_id)->float:
    '''Normalized Discounted Cumulative Gain'''
    for i in range(len(ranked_items)):
        if item_id == ranked_items[i]:
            return math.log(2) / math.log(i+2)
    return 0

def get_ranked_items(
    model,
    user_id:int,
    item_id:int,
    negative_transaction_table,
    num_neg:int=999
)->list:
    '''
    Arguments:
        model (tf.keras.models.Model): the trained GNMF model i.e. `GNMF.model`.
        user_id (int): id of the user being evaluated
        item_id (int): id of the item being evaluated
        negative_transaction_table (mkm.data.transactions.NegativeTransactionTable):
            class for looking up negative examples from the training data.
        num_neg (int): number of negative instances in which to consider.
            Defaults to 999.

    Returns:
        ranked_list (list): list of item ids in order of best to worse.
    '''
    negative_records = negative_transaction_table.lookup_negative_items(
        np.ones(num_neg, dtype=int)*user_id
    )
    evaluatation_items = np.concatenate((negative_records,[item_id]))

    scores = model.predict([
            np.ones(num_neg+1, dtype=int) * user_id,
            evaluatation_items
        ])
    probs = scores_to_probabilities(scores)
    ranking = np.flip(np.argsort(probs))
    sorted_items = evaluatation_items[ranking]
    return sorted_items


def evaluate_single_case(
    model,
    user_id:int,
    item_id:int,
    negative_transaction_table,
    num_neg:int=999,
    top_k:int=10,
)->tuple:
    '''
    Note:
        - it does not make sense for top_k = num_neg + 1

    Arguments:
        model (tf.keras.models.Model): the trained GNMF model i.e. `GNMF.model`.
        user_id (int): id of the user being evaluated
        item_id (int): id of the item being evaluated
        negative_transaction_table (mkm.data.transactions.NegativeTransactionTable):
            class for looking up negative examples from the training data.
        num_neg (int): number of negative instances in which to consider.
            Defaults to 999.
        top_k (int): how many items in which to test if the item is in the top k
            and ndcg scores. Defaults to 10.

    Returns:
        results (tuple): returns both the top_k and ndgc score.
    '''
    ranked_items = get_ranked_items(
        model, user_id, item_id,
        negative_transaction_table, num_neg
    )
    top_k = get_hit_ratio(ranked_items[:top_k], item_id)
    ndcg = get_ndcg(ranked_items[:top_k], item_id)
    return top_k, ndcg

def evaluate_test_set(
    model,
    users:list,
    items:list,
    negative_transaction_table,
    num_neg:int=999,
    top_k:int=10,
):
    '''
    Arguments:
        model (tf.keras.models.Model): the trained GNMF model i.e. `GNMF.model`.
        user_id (int): id of the user being evaluated
        item_id (int): id of the item being evaluated
        negative_transaction_table (mkm.data.transactions.NegativeTransactionTable):
            class for looking up negative examples from the training data.
        num_neg (int): number of negative instances in which to consider.
            Defaults to 999.
        top_k (int): how many items in which to test if the item is in the top k
            and ndcg scores. Defaults to 10.
    '''
    records = list(zip(users, items))
    topks = []
    ndcgs = []
    for (user_id, item_id) in records:
        topk, ndcg = evaluate_single_case(
            model, user_id, item_id, negative_transaction_table, num_neg, top_k
        )
        topks.append(topk)
        ndcgs.append(ndcg)
    return np.array(topks), np.array(ndcgs)
